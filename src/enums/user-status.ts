export enum UserStatus {
    VERIFIED = "VERIFIED",
    PENDING = "PENDING",
    DECLINED = "DECLINED",
    BLOCKED = "BLOCKED"
}