
export class Address {
    address: string = null;
    city: string = null;
    state: string = null;
    country: string = null;
    zipcode: string = null;

    constructor(data: Partial<Address>) {
        Object.keys(this).forEach(key => {
            if (key in data) {
                this[key] = data[key];
            }
            else {
                delete this[key];
            }
        });
    }
}