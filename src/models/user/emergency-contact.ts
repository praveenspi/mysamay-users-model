
export class EmergencyContact {
    name: string = null;
    mobile: string = null;
    relation: string = null;

    constructor(data: any) {
        Object.keys(this).forEach(key => {
            if (key in data) {
                this[key] = data[key];
            }
        });
    }
}