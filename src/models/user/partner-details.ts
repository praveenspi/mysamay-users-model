export class PartnerDetails {
    partnerName: string = null;
    userId: string = null;
    connectedTime: Date = null;

    constructor(data: Partial<PartnerDetails>) {
        Object.keys(this).forEach(key => {
            if (key in data) {
                this[key] = data[key];
            }
            else {
                delete this[key];
            }
        });
    }
}