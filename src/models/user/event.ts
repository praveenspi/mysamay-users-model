export class Event {
    eventId: string;
    eventCategory: string;
    targetDistance: number;
    clubId?: string;
    corporateId?: string;
}