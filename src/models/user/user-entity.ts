import { PartnerDetails } from './partner-details';
import { ClubEntity } from './club-entity';
import { CorporateEntity } from './corporate-entity';
import { UserStatus } from './../../enums/user-status';
import { EmergencyContact } from './emergency-contact';
import { Address } from './address';
import { MetadataHelper } from '@neb-sports/mysamay-common-utils';
import { Event } from "./event";

var dateFields = ["createdTime", "updatedTime", "birthDate"];

/**
 * Class to encapsulate End User Data.
 *
 * Default values for all fields are set to null.
 */
export class UserEntity {
    _id: string = null;
    given_name: string = null;
    family_name: string = null;
    displayName: string = null;
    email: string = null;
    mobile: string = null;
    roles: string[] = null;
    birthDate: Date = null;
    age: number = null;
    ageGroup: string = null;
    height: number = null;
    weight: number = null;
    profilePicture: string = null;
    tshirtSize: string = null;
    gender: string = null;
    bloodGroup: string = null;
    address: Address = null;
    emergencyContact: EmergencyContact = null;
    provider: string = null;
    socialSub: string = null;
    userStatus: UserStatus = null;
    corporate: CorporateEntity = null;
    club: ClubEntity = null;
    events: Event[] = [];
    partnerDetails: PartnerDetails[] = [];
    stepsTargetDaily: number = 1000;
    createdTime: Date = null;
    updatedTime: Date = null;

    constructor(data: Partial<UserEntity>) {
        Object.keys(this).forEach(key => {
            if (key in data) {
                if (dateFields.includes(key)) {
                    this[key] = new Date(data[key]);
                }
                else {
                    this[key] = data[key];
                }
            }
            else {
                delete this[key];
            }
        });
        if(!this._id) {
            this._id = MetadataHelper.generateUUID();
        }
        if(!this.createdTime) {
            this.createdTime = new Date();
        }
        if (!this.club) {
            this.club = {
                clubId: "",
                clubName: "" 
            };
        }
        if (!this.corporate) {
            this.corporate = {
                corporateId: "",
                corporateName: ""
            }
        }
        this.updatedTime = new Date();
        this.displayName = this.given_name + " " + this.family_name;
    }

    validate() {
        if(!this.email) {
            return false;
        }
        return true;
    }

    public findEvent(eventId: string) {
        if(this.events && this.events.length > 0) {
            for(let i =0; i < this.events.length; i++) {
                if(this.events[i].eventId === eventId) {
                    return i;
                }
            }
        }
        return -1;
    }

    public addOrUpdateEvent(event: Event) {
        if(this.events) {
            let index = this.findEvent(event.eventId);
            if(index === -1) {
                this.events.push(event);
                return false;
            }
            else {
                this.events[index] = event;
                return true;
            }
        }
        else {
            this.events = [event];
            return false;
        }
    }

    static collectionName = "Users_UserAccount";

}
