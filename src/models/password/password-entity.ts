import { MetadataHelper } from "@neb-sports/mysamay-common-utils";

var dateFields = ["createdTime", "updatedTime"];

export class PasswordEntity {
    _id: string = null;
    passwordHash: string = null;
    hashAlgo: string = null;
    userId: string = null;
    createdTime: Date = null;
    updatedTime: Date = null;

    constructor(data: Partial<PasswordEntity>) {
        Object.keys(this).forEach(key => {
            if (key in data) {
                if (dateFields.includes(key)) {
                    this[key] = new Date(data[key]);
                }
                else {
                    this[key] = data[key];
                }
            }
            else {
                delete this[key];
            }
        });
        if(!this._id) {
            this._id = MetadataHelper.generateUUID();
        }
        if(!this.createdTime) {
            this.createdTime = new Date();
        }
        this.updatedTime = new Date();
    }

    /**
     * Check if this object contains key.
     * @param key
     */
    containsKey(key: string): boolean {
        return Object.keys(this).includes(key);
    }

    static collectionName = "Users_PasswordHash";
}